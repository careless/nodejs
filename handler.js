const path = require('path')
const resource = require('./app/my_resource')

function get (event, _context, callback) {
  let entity = resource.get(event['pathParameters']['id'])

  let response = {
    'statusCode': 200,
    'headers': {
      'X-My-Custom-Header': 'CUSTOOOOOOOOOM!'
    },
    'body': JSON.stringify(entity)
  }

  callback(null, response)
}

function create (event, _context, callback) {
  let body = JSON.parse(event['body'])
  let entity = resource.create(body['name'])

  let response = {
    'statusCode': 200,
    'headers': {
      'Location': path.join(event['requestContext']['path'], String(entity['id'])),
      'X-My-Custom-Header': 'CUSTOOOOOOOOOM!'
    },
    'body': JSON.stringify(entity)
  }

  callback(null, response)
}

module.exports = {
  'get': get,
  'create': create
}
