#!/bin/bash

set -e

for var in "${@}"; do
    case "${var}" in
        init|deploy|test|lint|bash)
            ;;
        *)
            printf "Unknown option: %s\n" "${var}" >&2
            exit 1
            ;;
    esac
done

for var in "$@"; do
    case "$var" in
        "init")
            echo "---------------------------"
            echo "| INSTALLING DEPENDENCIES |"
            echo "---------------------------"
            npm install
            ;;

        "deploy")
            echo "-----------------"
            echo "| DEPLOY TO AWS |"
            echo "-----------------"

            if [ -z "$ENV" ]; then
                echo "ENV is undefined" >&2
                exit 1
            elif [[ "$ENV" = "local" ]]; then
                echo "ENV is local; can't deploy to local!" >&2
                exit 1
            fi

            serverless deploy --stage "$ENV" --verbose
            ;;

        "test")
            echo "-----------------"
            echo "| RUNNING TESTS |"
            echo "-----------------"
            npm run test-all
            ;;

        "lint")
            echo "------------------"
            echo "| RUNNING LINTER |"
            echo "------------------"
            npm run lint-all
            ;;

        "bash")
            echo "-----------------"
            echo "| RUNNING SHELL |"
            echo "-----------------"
            echo '>>> Try `run lint`, `run test` or `run <TAB>` <<<'
            /bin/bash --login
            ;;
    esac

    EXIT_CODE=$?

    [ $? -ne 0 ] && exit $EXIT_CODE
done

exit 0