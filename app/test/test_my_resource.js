const chai = require('chai')
const resource = require('../my_resource')

chai.should()

describe('my_resource', function () {
  describe('get', function () {
    it('returns an object', function () {
      resource.get(42).should.be.an('object')
    })

    it('returns an object with the expected id', function () {
      resource.get(42).should.have.property('id').that.equals(42)
    })

    it('returns an object with a string name', function () {
      resource.get(42).should.have.property('name').that.is.a('string')
    })
  })

  describe('create', function () {
    it('returns an object', function () {
      resource.create('foobar').should.be.an('object')
    })

    it('returns an object with an number id', function () {
      resource.create('foobar').should.have.property('id').that.is.a('number')
    })

    it('returns an object with the expected name', function () {
      resource.create('foobar').should.have.property('name').that.equals('foobar')
    })
  })
})
