function get (id) {
  return {
    'id': id,
    'name': `Master of ${id}`
  }
}

function create (name) {
  let id = Math.round(Math.random() * 1000000)

  return {
    'id': id,
    'name': name
  }
}

module.exports = {
  'get': get,
  'create': create
}
