DOCKER = ENV=$(ENV) docker-compose --file docker-compose.yml
DOCKER_RUN = $(DOCKER) run --rm function

DEPS = package.json
DEPS_INSTALLED = package-lock.json .installed

ENV ?= local


.PHONY: all
all: lint test deploy

.PHONY: shell
shell: $(DEPS_INSTALLED)
	$(DOCKER_RUN) bash

.PHONY: init
init $(DEPS_INSTALLED): $(DEPS) env/dev
	$(DOCKER) build
	$(DOCKER_RUN) init
	@touch $(DEPS_INSTALLED)

env/dev:
	$(warning Setting up env/dev with empty defaults; you'll need to update them for deployment!)

	@echo
	@touch env/dev
	@echo 'AWS_ACCESS_KEY_ID=' >> env/dev
	@echo 'AWS_SECRET_ACCESS_KEY=' >> env/dev

.PHONY: lint
lint: $(DEPS_INSTALLED)
	$(DOCKER_RUN) lint

.PHONY: test
test: $(DEPS_INSTALLED)
	$(DOCKER_RUN) test

.PHONY: deploy
deploy: $(DEPS_INSTALLED)
	$(DOCKER_RUN) deploy

.PHONY: clean
clean:
	$(DOCKER) down --volumes
	@touch .installed
	@rm .installed