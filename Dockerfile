FROM lambci/lambda:build-nodejs8.10

WORKDIR /function

RUN npm install --global serverless

RUN \
  echo 'run() ( cd /function && ./docker-entrypoint.sh $@; )' >> ~/.bash_profile &&\
  echo 'complete -W "deploy init lint test" run' >> ~/.bash_profile

ENTRYPOINT ["./docker-entrypoint.sh"]
CMD ["init", "bash"]