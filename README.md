# JavaScript Function

Based on the [docker-lambda][docker-lambda] image for NodeJS 8.10. It comes with the following libraries preinstalled:
- [`aws-sdk`][js-aws-sdk] for obvious reasons
- [`eslint`][js-eslint] and a number of plugins for linting
- [`mocha`][js-mocha] and [`chai`][js-chai] for testing

As would be expected it uses `npm` for dependency management. In case you need an additional dependency you can simply run `npm install <dependency>` in the container (for dev dependencies add `--save-dev`); `npm` will automatically update `package.json`.

There are no additional `serverless` plugins installed.

## `make`

*[For a rough overview take a look at the main README.](/README.md)*

Let's break down what the `Makefile` for JavaScript does exactly.

### `make init`

__Depends on:__  `package.json` and `env/dev` (if missing, get's generated with empty default variables for AWS credentials)

#### Tasks
- `docker-compose build`
- `docker-compose run --rm function init` - runs `npm install` in the container ([docker-entrypoint.sh](docker-entrypoint.sh#L18-L23))
- `touch package-lock.json .installed` - `.installed` is acts as a "flat" that the initialization happened and is being ignored by git

### `make shell`

__Depends on:__ init

#### Tasks
- `docker-compose run --rm function bash` - runs `/bin/bash --login` in the container ([docker-entrypoint.sh](docker-entrypoint.sh#L55-L61))

### `make lint`

__Depends on:__ init

#### Tasks
- `docker-compose run --rm function lint` - runs `./node_modules/.bin/eslint handler.js app/` in the container ([docker-entrypoint.sh](docker-entrypoint.sh#L48-L53))

### `make test`

__Depends on:__ init

#### Tasks
- `docker-compose run --rm function test` - runs `npm test` in the container ([docker-entrypoint.sh](docker-entrypoint.sh#L41-L46))

### `make deploy`

__Depends on:__ init

#### Tasks
- `docker-compose run --rm function test` ([docker-entrypoint.sh](docker-entrypoint.sh#L25-L39))
  - ensures that `ENV` is set (and not `local`)
  - runs `serverless deploy --stage "$ENV" --verbose`


[docker-lambda]: https://github.com/lambci/docker-lambda
[js-aws-sdk]: https://aws.amazon.com/sdk-for-node-js/
[js-eslint]: https://eslint.org/
[js-mocha]: https://mochajs.org/
[js-chai]: http://www.chaijs.com/
